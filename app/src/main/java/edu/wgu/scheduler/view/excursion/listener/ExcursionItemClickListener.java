package edu.wgu.scheduler.view.excursion.listener;

import edu.wgu.scheduler.model.Excursion;

/**
 * Listener for when an excursion is clicked.
 */
public interface ExcursionItemClickListener {
    void onDeleteButtonClick(Excursion excursion);
    void onDetailsButtonClick(Excursion excursion);
}
