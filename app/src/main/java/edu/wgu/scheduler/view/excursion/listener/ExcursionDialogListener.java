package edu.wgu.scheduler.view.excursion.listener;

import edu.wgu.scheduler.model.Excursion;
import edu.wgu.scheduler.view.dialog.DialogListener;

/**
 * Base interface for all excursion dialog listeners.
 */
public interface ExcursionDialogListener extends DialogListener {
    void addExcursion(Excursion excursion);
    void editExcursion(Excursion excursion);
}
