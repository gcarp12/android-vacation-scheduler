package edu.wgu.scheduler.view.vacation.listener;

import androidx.annotation.NonNull;

import edu.wgu.scheduler.model.Vacation;
import edu.wgu.scheduler.view.dialog.DialogListener;

/**
 * Base interface for all vacation dialog listeners.
 */
public interface VacationDialogListener extends DialogListener {
    void addVacation(@NonNull Vacation vacation);
    void editVacation(@NonNull Vacation vacation);
}
