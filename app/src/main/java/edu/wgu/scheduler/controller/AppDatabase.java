package edu.wgu.scheduler.controller;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import edu.wgu.scheduler.controller.convertor.DateConverter;
import edu.wgu.scheduler.controller.dao.ExcursionDao;
import edu.wgu.scheduler.controller.dao.VacationDao;
import edu.wgu.scheduler.model.Excursion;
import edu.wgu.scheduler.model.Vacation;

/**
 * The Room database for the application.
 */
@Database(entities = {Vacation.class, Excursion.class}, version = 1, exportSchema = false)
@TypeConverters({DateConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract VacationDao vacationDao();
    public abstract ExcursionDao excursionDao();

}
